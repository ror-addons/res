--Initialise variables local to the addon
local CAST_BEGIN = true
local CAST_FAILED = false
local DEBUG_ON = false	-- Set to true to display debug messages in chat window
local DEBUGLEVEL = 3		-- Set the debug level (3 = Positiion, 2 = Setting values, 1 = Return values, 0 = Illegal flow catches)
local EnableModCheckToggle  = false
local EnableButtonCheckToggle = false
local EnableCastCheckToggle = false
local T = LibStub("WAR-AceLocale-3.0"):GetLocale("Res",DEBUG_ON)
local VERSION = 1.3		-- Set version of app for future changes/comparisons.

--local function debugMsg(str,lvl)
    -- If debug is enabled and the level of the message is high enough then print the string to the chat window
--	if (DEBUG_ON and lvl <= DEBUGLEVEL) then EA_ChatWindow.Print(towstring(L"Res(DEBUG): "..str)) end
--end

local function printMsg(str)
	EA_ChatWindow.Print(towstring(L"Res: "..str))
end

--Check if the Res object already exists and if not create it
if (not Res) then
	Res = {}
	Res.Ability = {}
	Res.AbilityExists = false
	Res.AbilityList = {
			[1598] = "Rune Of Life"
		, 	[1908] = "Gedup!"
		, 	[8248] = "Breath Of Sigmar"
		, 	[8555] = "Tzeentch Shall Remake You"
		, 	[9246] = "Gift Of Life"
		, 	[9558] = "Stand, Coward!"
	                  }
	Res.Button = {}
	Res.Button.Window = "ResToggle"
	Res.Button.Btn = Res.Button.Window.."Button"
	Res.Button.Image = Res.Button.Window.."Image"
	Res.Channels = {
--			{ channel = "d"  , desc = "Default"       , id = SystemData.ChatLogFilters.SAY}
		 	{ channel = "sm" , desc = "Smart Channel" , id = SystemData.ChatLogFilters.SAY}
		, 	{ channel = "p"  , desc = "Party"         , id = SystemData.ChatLogFilters.GROUP}
		, 	{ channel = "wb" , desc = "Warband"       , id = SystemData.ChatLogFilters.BATTLEGROUP}
		, 	{ channel = "sc" , desc = "Scenario"      , id = SystemData.ChatLogFilters.SCENARIO}
		,	{ channel = "s"  , desc = "Say"           , id = SystemData.ChatLogFilters.SAY}
		,	{ channel = "sh" , desc = "Shout"         , id = SystemData.ChatLogFilters.SHOUT}
		,	{ channel = "g"  , desc = "Guild"         , id = SystemData.ChatLogFilters.GUILD}
		,	{ channel = "as" , desc = "Alliance"      , id = SystemData.ChatLogFilters.ALLIANCE}
		,	{ channel = "1"  , desc = "Region 1"      , id = SystemData.ChatLogFilters.CHANNEL_1}
		,	{ channel = "2"  , desc = "Region 2"      , id = SystemData.ChatLogFilters.CHANNEL_2}
		,	{ channel = "t1" , desc = "Tier 1"        , id = SystemData.ChatLogFilters.REALM_WAR_T1}
		,	{ channel = "t2" , desc = "Tier 2"        , id = SystemData.ChatLogFilters.REALM_WAR_T2}
		,	{ channel = "t3" , desc = "Tier 3"        , id = SystemData.ChatLogFilters.REALM_WAR_T3}
		,	{ channel = "t4" , desc = "Tier 4"        , id = SystemData.ChatLogFilters.REALM_WAR_T4}
	               }
	Res.CastingRegistered  = false
	Res.CheckName = ""
	Res.Enabled = true
	Res.Icon = {}
	Res.Icon.Backgrounds = {
		   { texture = "ResBackTexture" , slice = "Grey"   , name = "Grey"  }
		 , { texture = "ResBackTexture" , slice = "Orange" , name = "Orange"}
		 , { texture = "ResBackTexture" , slice = "Yellow" , name = "Yellow"}
		 , { texture = "ResBackTexture" , slice = "Green"  , name = "Green" }
		 , { texture = "ResBackTexture" , slice = "Red"    , name = "Red"   }
		 , { texture = "ResBackTexture" , slice = "Blue"   , name = "Blue"  }
		 , { texture = "ResBackTexture" , slice = "Purple" , name = "Purple"}
		 , { texture = "ResBackTexture" , slice = "Pink"   , name = "Pink"  }
	                       }
	Res.Icon.Foregrounds = {
		   { texture = "ResForeTexture" , slice = "Black"     , name = "Black"     }
		 , { texture = "ResForeTexture" , slice = "DarkRed"   , name = "DarkRed"   }
		 , { texture = "ResForeTexture" , slice = "DarkGreen" , name = "DarkGreen" }
		 , { texture = "ResForeTexture" , slice = "Orange"    , name = "Orange"    }
		 , { texture = "ResForeTexture" , slice = "White"     , name = "White"     }
		 , { texture = "ResForeTexture" , slice = "Red"       , name = "Red"       }
		 , { texture = "ResForeTexture" , slice = "Green"     , name = "Green"     }
		 , { texture = "ResForeTexture" , slice = "Yellow"    , name = "Yellow"    }
								  }
	Res.Options = {}
	Res.Options.Window = "ResOptions"
	Res.Options.Icon = {}
	Res.Options.Icon.Name   = Res.Options.Window.."TestIcon"
	Res.Options.Icon.Button = Res.Options.Icon.Name.."Button"
	Res.Options.Icon.Image  = Res.Options.Icon.Name.."Image"
	Res.Options.Icon.Background = 0
	Res.Options.Icon.Foreground = 0
	Res.Options.Icon.Highlight  = 0
end

-------------------------------------------------
----------    Core Addon Functions   ------------
-------------------------------------------------

-- This function is called whenever the addon is loaded.
function Res.Init()
	--debugMsg(L"Entering Init",3)
	-- Check to see if any options are saved on the users profile.  If not set default values.
	if (not Res.Settings) then
		Res.Settings = {}
		Res.Settings.Channel = 1
		Res.Settings.Enabled = true
		Res.Settings.Message = ""
		Res.Settings.ShowButton = true
		Res.Settings.version = 1.0
	end
	-- Check to see if earlier version of Res has already been installed and if so,
	-- add the new settings parameters required for version 1.1
	if (Res.Settings.version < 1.21) then
		Res.Settings.Icon = {}
		Res.Settings.Icon.Background = 7
		Res.Settings.Icon.Foreground = 1
		Res.Settings.Icon.Highlight  = 5
		Res.Settings.CheckCasting = true
		Res.Settings.version = 1.21
	end
	Res.Settings.version = VERSION
	-- Save a local version of enabled flag so that Res can turn itself off if a none healer
   -- character is loaded, without the disable affecting user profile settings.
	Res.Enabled = Res.Settings.Enabled
	-- Register the slash commands
	LibSlash.RegisterSlashCmd("res", Res.Slash)
	-- If Res isn't turned off in the settings then add hook to the player cast event
	if (Res.Enabled) then
		Res.RegisterEvents()
		printMsg(T["Enabled. Type /res to view the options"])
	else
		printMsg(T["Disabled. Type /res to view the options"])
	end
	-- Iniitalise the visual windows
	Res.OptionsWindowInit()
	Res.ToggleButtonInit()
   --debugMsg(L"Leaving Init",3)
end

function Res.RegisterEvents()
	--debugMsg(L"Entering RegisterEvents",3)
	RegisterEventHandler(SystemData.Events.PLAYER_BEGIN_CAST, "Res.StartCast")
   --debugMsg(L"Leaving RegisterEvents",3)
end

function Res.UnRegisterEvents()
	--debugMsg(L"Entering UnRegisterEvents",3)
	UnregisterEventHandler(SystemData.Events.PLAYER_BEGIN_CAST, "Res.StartCast")
	if Res.CastingRegistered then
		UnregisterEventHandler(SystemData.Events.PLAYER_END_CAST, "Res.EndCast")
	end
	--debugMsg(L"Leaving UnRegisterEvents",3)
end

function Res.FindResAbility()
	--debugMsg(L"Entering FindResAbility",3)
	-- Retrieve the players abilities
	local abilityTable = GetAbilityTable(GameData.AbilityType.STANDARD)
	-- Traverse through the abilities looking for one of the 6 resurrection abilities
	for i, v in pairs(Res.AbilityList) do
		if abilityTable[i] ~= nil then
			--debugMsg(L"FindResAbility: Ability found ["..abilityTable[i]["name"]..L"]",2)
			-- A ressurection ability has been located, store its details and flag it's existance.
			Res.Ability[i] = abilityTable[i]["name"]
			printMsg(T["Resurrection found: "]..abilityTable[i]["name"]);
			Res.AbilityExists = true
			--debugMsg(L"Leaving FindResAbility",3)
			return
		end
   end
	--debugMsg(L"FindResAbility: Ability not found",2)
	-- No ressurection ability has been located.
	Res.AbilityExists = false
	--debugMsg(L"Leaving FindResAbility",3)
end

function Res.StartCast(abilityId, isChanneledSpell, desiredCastTime, averageLatency)
	--debugMsg(L"Entering StartCast",3)
   -- Check to see if Res should be Announceing or not
	if (Res.Enabled and not Res.AbilityExists) then
		-- Check to see if the character has a res ability
		Res.FindResAbility()
		if (not Res.AbilityExists) then
			-- No ressurection ability found, so disable mod, unhook and return.
			Res.Enabled = false
			Res.UnRegisterEvents()
			printMsg(T["A resurrection ability was not found. Res has turned itself off."]);
			return
		end
	end
	--debugMsg(L"Casting Stats: ",2)
	--debugMsg(L"Player Name ["..GameData.Player.name..L"]",2)
	--debugMsg(L"Ability Id  ["..Res.Ability[abilityId]..L"]",2)
	--debugMsg(L"Target info ["..TargetInfo:UnitName("selffriendlytarget")..L"]",2)

	-- Only announce Resurrection if:
	-- the ability being cast is the Res ability and
	-- a friendly target has been targeted and
	-- player has not targeted themself
	if ( Res.Ability[abilityId] ~= nil
		  and TargetInfo:UnitName("selffriendlytarget") ~= L""
		  and TargetInfo:UnitName("selffriendlytarget") ~= GameData.Player.name
	   ) then
		-- A valid ressurection is being cast, announce it in chat.
		Res.Announce(CAST_BEGIN)
		-- Now set the addon to monitor the cast to report if it succeeded or not.
		if(Res.Settings.CheckCasting) then
			Res.CastingRegistered = true
			RegisterEventHandler (SystemData.Events.PLAYER_END_CAST, "Res.EndCast")
		end
	end
	--debugMsg(L"Leaving StartCast",3)
end

function Res.EndCast(CastFailed)
	 --debugMsg(L"Entering EndCast",3)
	if Res.Settings.CheckCasting and CastFailed then
		Res.Announce(CAST_FAILED)
	end
	UnregisterEventHandler(SystemData.Events.PLAYER_END_CAST, "Res.EndCast")
	Res.CastingRegistered = false
	--debugMsg(L"Leaving EndCast",3)
end

function Res.Announce(ResCastBegin)
	 --debugMsg(L"Entering Announce",3)
	--local ResCastBeginText = if (ResCastBegin) then L"True" else L"False" end
	----debugMsg(L"Announce: ResCastBegin["..ResCastBeginText..L"]",2)
	-- Temp code for debugging Smart channel decisions.
	-- local IsInSC = if (GameData.Player.isInScenario) then L"True" else L"False" end
	-- local IsInWB, IsInParty, IsInGroup = "", "", "", ""
	-- if IsWarBandActive()            then IsInWB    = L"True" else IsInWB    = L"False" end
	-- if PartyUtils.IsPartyActive()   then IsInParty = L"True" else IsInParty = L"False" end
	-- if GroupWindow.inWorldGroup     then IsInGroup = L"True" else IsInGroup = L"False" end
	----debugMsg(L"In a SC?    ["..IsInSC..L"]",2)
	----debugMsg(L"In a WB?    ["..IsInWB..L"]",2)
	----debugMsg(L"In a Party? ["..IsInParty..L"]",2)
	----debugMsg(L"In a Group? ["..IsInGroup..L"]",2)
	-- Check to see if default channel is selected and if not set chat to the desired channel
	local message = ""
	local channel = towstring(Res.Channels[Res.Settings.Channel].channel)
	local channelID = Res.Channels[Res.Settings.Channel].id
	--debugMsg(L"Announce: Channel["..channel..L"]",2)
	if channel == L"sm" then
	   --debugMsg(L"Announce: Smart channel detected",2)
		-- Ascertain users status and set to appropriate channel.
		if GameData.Player.isInScenario then
			message = ChatSettings.Channels[SystemData.ChatLogFilters.SCENARIO].serverCmd
		elseif IsWarBandActive() then
			message = ChatSettings.Channels[SystemData.ChatLogFilters.BATTLEGROUP].serverCmd
		elseif PartyUtils.IsPartyActive() then
			message = ChatSettings.Channels[SystemData.ChatLogFilters.GROUP].serverCmd
		else
			message = ChatSettings.Channels[SystemData.ChatLogFilters.SAY].serverCmd
		end
	elseif channel == L"d" then
		message = ChatSettings.Channels[EA_ChatWindow.curChannel].serverCmd
		if EA_ChatWindow.curChannel == SystemData.ChatLogFilters.TELL_SEND then
			message = message..L" "..EA_ChatWindow.whisperTarget
		elseif EA_ChatWindow.curChannel == SystemData.ChatLogFilters.ADVICE then
			-- Check to see if user is going to announce on the Advice channel
			-- If so redirect to the /say channel instead
			message = ChatSettings.Channels[SystemData.ChatLogFilters.SAY].serverCmd
		end
	else
		message = ChatSettings.Channels[channelID].serverCmd
	end

	-- Set the text to be sent
	if ResCastBegin then
		Res.CheckName = TargetInfo:UnitName("selffriendlytarget")
		message = message..L" "..T["Resurrecting "]..Res.CheckName..L" "..towstring(Res.Settings.Message)		
	else
		message = message..L" "..T["Resurrection of "]..Res.CheckName..T[" was unsuccessful."]
	end
	-- Send the text
	SendChatText(towstring(message),L"")
	--debugMsg(L"Leaving Announce",3)
end

function Res.Shutdown()
	--debugMsg(L"Entering Shutdown",3)
	-- Mod is shutdown, unsubscribe from events.
	Res.UnRegisterEvents()
	--debugMsg(L"Leaving Shutdown",3)
end

function Res.Slash()
	--debugMsg(L"Entering Slash",3)
	-- When user types /res show the Options window.
	Res.ShowOptions()
	--debugMsg(L"Leaving Slash",3)
end

-------------------------------------------------
---------- Window Specific Functions ------------
-------------------------------------------------

-----------------------------
-- Option Window Functions --
-----------------------------
function Res.OptionsWindowInit()
	--debugMsg(L"Entering OptionsWindowInit",3)
	-- Create and register the options window and then hide it.
	CreateWindow(Res.Options.Window, true)
	WindowSetShowing(Res.Options.Window,false)
	--debugMsg(L"OptionsWindowInit: Window created",3)
	-- Set Label values
	LabelSetText(Res.Options.Window.."TitleBarText",             T["Res Options"]..L" v"..towstring(Res.Settings.version))
	LabelSetText(Res.Options.Window.."EnableModLabel",           T["Enable the Res addon"])
	LabelSetText(Res.Options.Window.."EnableToggleLabel",        T["Show the toggle button"])
	LabelSetText(Res.Options.Window.."EnableCastLabel",          T["Check casting success"])	
	LabelSetText(Res.Options.Window.."ChooseChannelsLabel",      T["Select a channel"])
	LabelSetText(Res.Options.Window.."ChooseMessageLabel",       T["Type a custom message"])
	LabelSetText(Res.Options.Window.."SelectorsLabel",           T["Icon Customisation:"])
	LabelSetText(Res.Options.Window.."SelectorsLabelBackground", T["Select a background colour"])
	LabelSetText(Res.Options.Window.."SelectorsLabelForeground", T["Select a foreground colour"])
	LabelSetText(Res.Options.Window.."SelectorsLabelHighlight" , T["Select a highlight colour"])
	LabelSetText(Res.Options.Window.."TestIconLabel",            T["Sample Icon"])
	-- Set Button labels
	ButtonSetText(Res.Options.Window.."SaveButton",  T["Save"])
	ButtonSetText(Res.Options.Window.."CloseButton", T["Close"])
	-- Set the channel descriptions to the appropriate combo box
   for i, v in ipairs(Res.Channels) do
		ComboBoxAddMenuItem(Res.Options.Window.."ChooseChannelsComboBox", towstring(v.desc))
   end
	-- Set the foreground and highlight names to the appropriate combo box
   for i, v in ipairs(Res.Icon.Backgrounds) do
		ComboBoxAddMenuItem(Res.Options.Window.."SelectorsComboBoxBackground", towstring(v.name))
   end
	-- Set the foreground and highlight names to the appropriate combo box
   for i, v in ipairs(Res.Icon.Foregrounds) do
		ComboBoxAddMenuItem(Res.Options.Window.."SelectorsComboBoxForeground", towstring(v.name))
		ComboBoxAddMenuItem(Res.Options.Window.."SelectorsComboBoxHighlight",  towstring(v.name))
	end
	--debugMsg(L"Leaving OptionsWindowInit",3)
end

function Res.ShowOptions()
	--debugMsg(L"Entering ShowOptions",3)
	-- Enable Addon CheckBox Setting stored in Res.Enabled
	EnableModCheckToggle = Res.Settings.Enabled
	ButtonSetPressedFlag(Res.Options.Window.."EnableModCheckBox",EnableModCheckToggle)
	-- Enable Toggle Button CheckBox Setting stored in Res.Settings.ShowButton
	EnableButtonCheckToggle = Res.Settings.ShowButton
	ButtonSetPressedFlag(Res.Options.Window.."EnableToggleCheckBox",EnableButtonCheckToggle)
	-- Enable Checking to see if casting of spell was successful stored in Res.Settings.CheckCasting
	EnableCastCheckToggle = Res.Settings.CheckCasting
	ButtonSetPressedFlag(Res.Options.Window.."EnableCastCheckBox",EnableCastCheckToggle)
	-- Show the selected channel in the combobox stored in Res.Settings.Channel
	ComboBoxSetSelectedMenuItem(Res.Options.Window.."ChooseChannelsComboBox", Res.Settings.Channel)
	-- Custom Message setting stored in Res.Settings.Message
	TextEditBoxSetText(Res.Options.Window.."ChooseMessageEditBox", Res.Settings.Message)
	--Store a copy of the icons saved settings for manipulation in the options window.
	Res.Options.Icon.Background = Res.Settings.Icon.Background
	Res.Options.Icon.Foreground = Res.Settings.Icon.Foreground
	Res.Options.Icon.Highlight  = Res.Settings.Icon.Highlight
	-- Show the selected channel in the combobox stored in Res.Settings.Channel
	ComboBoxSetSelectedMenuItem(Res.Options.Window.."SelectorsComboBoxBackground", Res.Options.Icon.Background)
	ComboBoxSetSelectedMenuItem(Res.Options.Window.."SelectorsComboBoxForeground", Res.Options.Icon.Foreground)
	ComboBoxSetSelectedMenuItem(Res.Options.Window.."SelectorsComboBoxHighlight" , Res.Options.Icon.Highlight)
   -- Apply the Icon settings to the icon.
	Res.OptionsIconSetTextures()
	-- Now that all of the settings have been set, show the Options window.
	WindowSetShowing(Res.Options.Window,true)
	--debugMsg(L"Leaving ShowOptions",3)
end

function Res.OptionsIconSetTextures()
	--debugMsg(L"Entering TestButtonSetTextures",3)
	-- Set the background texture and slice for the dummy Toggle Button in the Options window
	DynamicImageSetTexture( Res.Options.Icon.Image
	                      , Res.Icon.Backgrounds[Res.Options.Icon.Background].texture
						  , 0, 0
						  )
   DynamicImageSetTextureSlice( Res.Options.Icon.Image
	                           , Res.Icon.Backgrounds[Res.Options.Icon.Background].slice
										)
	-- Set the foreground texture and slice for the dummy Toggle Button for both normal and highlighted conditions.
	ButtonSetTextureSlice( Res.Options.Icon.Button
	                     , Button.ButtonState.NORMAL
							   , Res.Icon.Foregrounds[Res.Options.Icon.Foreground].texture
							   , Res.Icon.Foregrounds[Res.Options.Icon.Foreground].slice
							   )
	ButtonSetTextureSlice( Res.Options.Icon.Button
	                     , Button.ButtonState.HIGHLIGHTED
							   , Res.Icon.Foregrounds[Res.Options.Icon.Highlight].texture
							   , Res.Icon.Foregrounds[Res.Options.Icon.Highlight].slice
							   )
	--debugMsg(L"Leaving TestButtonSetTextures",3)
end

function Res.OptionsIconBackgroundSelected()
	--debugMsg(L"Entering OptionsIconBackgroundSelected",3)
	-- As soon as the user selects a different background colour apply it to the OptionsIcon
	Res.Options.Icon.Background = ComboBoxGetSelectedMenuItem(Res.Options.Window.."SelectorsComboBoxBackground")
	Res.OptionsIconSetTextures()
	--debugMsg(L"Leaving OptionsIconBackgroundSelected",3)
end

function Res.OptionsIconForegroundSelected()
	--debugMsg(L"Entering OptionsIconForegroundSelected",3)
	-- As soon as the user selects a different foreground colour apply it to the OptionsIcon
	Res.Options.Icon.Foreground = ComboBoxGetSelectedMenuItem(Res.Options.Window.."SelectorsComboBoxForeground")
	Res.OptionsIconSetTextures()
	--debugMsg(L"Leaving OptionsIconForegroundSelected",3)
end

function Res.OptionsIconHighlightSelected()
	--debugMsg(L"Entering OptionsIconHighlightSelected",3)
	-- As soon as the user selects a different highlight colour apply it to the OptionsIcon
	Res.Options.Icon.Highlight = ComboBoxGetSelectedMenuItem(Res.Options.Window.."SelectorsComboBoxHighlight")
	Res.OptionsIconSetTextures()
	--debugMsg(L"Leaving OptionsIconHighlightSelected",3)
end

function Res.SaveOptions()
	--debugMsg(L"Entering SaveOptions",3)
	--Retrieve the settings from the option window and save them to Res.Settings
	Res.Settings.Enabled = ButtonGetPressedFlag( Res.Options.Window.."EnableModCheckBox" )
	-- Check to see if the user has just changed the enable status of the mod and if so, alter the mod accordingly
	if (Res.Enabled ~= Res.Settings.Enabled) then
		Res.Toggle()
	end
	Res.Settings.CheckCasting = ButtonGetPressedFlag( Res.Options.Window.."EnableCastCheckBox" )
	-- Store channel and message for later use in Announce function.
	Res.Settings.Channel = ComboBoxGetSelectedMenuItem(Res.Options.Window.."ChooseChannelsComboBox")
	Res.Settings.Message = TextEditBoxGetText(Res.Options.Window.."ChooseMessageEditBox")
	-- Store icon settings and apply them
	Res.Settings.Icon.Background = ComboBoxGetSelectedMenuItem(Res.Options.Window.."SelectorsComboBoxBackground")
	Res.Settings.Icon.Foreground = ComboBoxGetSelectedMenuItem(Res.Options.Window.."SelectorsComboBoxForeground")
	Res.Settings.Icon.Highlight  = ComboBoxGetSelectedMenuItem(Res.Options.Window.."SelectorsComboBoxHighlight")
	Res.ToggleButtonSetTextures()
	-- Show/Hide the toggle button as the user has chosen
	Res.Settings.ShowButton = ButtonGetPressedFlag(Res.Options.Window.."EnableToggleCheckBox")
	WindowSetShowing(Res.Button.Window,Res.Settings.ShowButton)
	--debugMsg(L"Leaving SaveOptions",3)
end

function Res.CloseOptions()
	--debugMsg(L"Entering CloseOptions",3)
	-- Hide the Options window
	WindowSetShowing(Res.Options.Window,false)
	--debugMsg(L"Leaving CloseOptions",3)
end

-----------------------------
-- Toggle Button Functions --
-----------------------------
function Res.ToggleButtonInit()
	--debugMsg(L"Entering ToggleButtonInit",3)
	-- Create and register the toggle button window and then show/hide it based on the settings.
	CreateWindow(Res.Button.Window, true)
	-- This is not a typo, for some reason the ButtonSetTextureSlice function only works if
	-- the window is first hidden and then shown, no idea why and it was a pain to debug.
	WindowSetShowing(Res.Button.Window,false)
	WindowSetShowing(Res.Button.Window,Res.Settings.ShowButton)
	--debugMsg(L"ToggleButtonInit: Window created",3)
	-- Register window with the layout editor so that it can be moved and resized there.
	LayoutEditor.RegisterWindow ( Res.Button.Window			-- windowName
                               , L"Res Button" 				-- windowDisplayName
                               , L"The toggle button for Res"	-- windowDesc
                               , false 						-- allowSizeWidth
                               , false 						-- allowSizeHeight
                               , true  						-- allowHiding
                               , nil 							-- allowableAnchorList
                               )
	Res.ToggleButtonSetTextures()
	--debugMsg(L"Leaving ToggleButtonInit",3)
end

function Res.ToggleButtonSetTextures()
	--debugMsg(L"Entering ToggleButtonSetTextures",3)
	-- Set the background texture and slice for the Toggle Button
	DynamicImageSetTexture( Res.Button.Image
	                      , Res.Icon.Backgrounds[Res.Settings.Icon.Background].texture
								 , 0, 0
								 )
   DynamicImageSetTextureSlice( Res.Button.Image
	                           , Res.Icon.Backgrounds[Res.Settings.Icon.Background].slice
										)
	-- Set the foreground texture and slice for the Toggle Button for both normal and highlighted conditions.
	ButtonSetTextureSlice( Res.Button.Btn
								, Button.ButtonState.NORMAL
								, Res.Icon.Foregrounds[Res.Settings.Icon.Foreground].texture
								, Res.Icon.Foregrounds[Res.Settings.Icon.Foreground].slice
								)
	ButtonSetTextureSlice( Res.Button.Btn
	                     , Button.ButtonState.HIGHLIGHTED
							   , Res.Icon.Foregrounds[Res.Settings.Icon.Highlight].texture
							   , Res.Icon.Foregrounds[Res.Settings.Icon.Highlight].slice
							   )
	--debugMsg(L"Leaving ToggleButtonSetTextures",3)
end

function Res.Toggle()
	--debugMsg(L"Entering Toggle",3)
	-- Check the status of Res and alter it to the opposite
	if (Res.Enabled) then
		-- Disable Res
		Res.UnRegisterEvents()
		Res.Settings.Enabled = false
		Res.Enabled = false
	else
		Res.Settings.Enabled = true
		Res.FindResAbility()
		-- Enable Res only if resurrection ability exists
		if (Res.AbilityExists) then
			Res.RegisterEvents()
			Res.Enabled = true
		else
			printMsg(T["Ressurection ability not found. Res has turned itself back off."]);
			Res.Enabled = false
		end
	end
	Res.ToggleButtonMouseOver()
	--debugMsg(L"Leaving Toggle",3)
end

function Res.ToggleButtonMouseOver()
	--debugMsg(L"Entering ToggleButtonMouseOver",3)
	local TextStatus = L""
	local TextToggle = L""
	local TextAbility = L""
	local TextChannel = towstring(Res.Channels[Res.Settings.Channel].desc)
	--debugMsg(L"ToggleButtonMouseOver: Vairables initialised",3)
	-- Generate the tooltip message
	if (Res.Enabled) then
		 TextStatus = T["Enabled"]
		 TextToggle = T["Disable"]
	else
		 TextStatus = T["Disabled"]
		 TextToggle = T["Enable"]
	end
	if (not Res.AbilityExists) then
		TextAbility = T["Note: No resurrection ability has currently been located"]
	end

	local msg = L"\n"..T["Destination chat channel is: "]..TextChannel
	msg = msg..L"\n"..T["Your current custom message is: "]..towstring(Res.Settings.Message)
	--debugMsg(L"ToggleButtonMouseOver: Active window name ["..towstring(SystemData.ActiveWindow.name)..L"]",2)
	if (SystemData.ActiveWindow.name == Res.Button.Btn ) then
		msg = msg..L"\n\n"..T["Left Click to "]..TextToggle..T[" Res"]
		msg = msg..L"\n"..T["Right Click to view the Options Window"]
	end
	--debugMsg(L"ToggleButtonMouseOver: Msg created",3)
	-- Set the tooltip message
	Tooltips.CreateTextOnlyTooltip( SystemData.ActiveWindow.name, nil)
	Tooltips.SetTooltipText (1, 1, T["Res is currently "]..TextStatus)
	if (Res.Enabled) then
		Tooltips.SetTooltipColor(1, 1, 0, 255, 0)
	else
		Tooltips.SetTooltipColor(1, 1, 255, 0, 0)
	end
	Tooltips.SetTooltipText (2, 1, TextAbility)
	Tooltips.SetTooltipText (3, 1, towstring(msg))
	Tooltips.AnchorTooltip(Tooltips.ANCHOR_WINDOW_BOTTOM)
	Tooltips.Finalize()
	--debugMsg(L"Leaving ToggleButtonMouseOver",3)
end

-----------------------------
--     Misc. Functions     --
-----------------------------
-- Can't workout how to keep check box ticked via game functionality so here is a manual work around.
-- TODO: Locate the in built Mythic code to do this, it must exist somewhere.
function Res.EnableModCheckToggle()
	--debugMsg(L"Entering EnableModCheckToggle",3)
	EnableModCheckToggle = not EnableModCheckToggle
	ButtonSetPressedFlag(Res.Options.Window.."EnableModCheckBox",EnableModCheckToggle)
	--debugMsg(L"Leaving EnableModCheckToggle",3)
end

function Res.EnableButtonCheckToggle()
	--debugMsg(L"Entering EnableButtonCheckToggle",3)
	EnableButtonCheckToggle = not EnableButtonCheckToggle
	ButtonSetPressedFlag(Res.Options.Window.."EnableToggleCheckBox",EnableButtonCheckToggle)
	--debugMsg(L"Leaving EnableButtonCheckToggle",3)
end

function Res.EnableCastCheckToggle()
	--debugMsg(L"Entering EnableCastCheckToggle",3)
	EnableCastCheckToggle = not EnableCastCheckToggle
	ButtonSetPressedFlag(Res.Options.Window.."EnableCastCheckBox",EnableCastCheckToggle)
	--debugMsg(L"Leaving EnableCastCheckToggle",3)
end